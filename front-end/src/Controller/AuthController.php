<?php

namespace App\Controller;

use App\Entity\User;
use FOS\UserBundle\Controller\SecurityController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class AuthController
 * @package App\Controller
 */
class AuthController extends SecurityController
{

    /** @var null|CsrfTokenManagerInterface $tokenManager */
    private $tokenManager;

    /**
     * AuthController constructor.
     * @param CsrfTokenManagerInterface|null $tokenManager
     *
     */
    public function __construct(
        CsrfTokenManagerInterface $tokenManager = null
    )
    {
        $this->tokenManager = $tokenManager;
    }

    /**
     * @param Request $request
     * @return Response
     * @throws \Facebook\Exceptions\FacebookSDKException
     */
    public function loginAction(Request $request)
    {
        /** @var $session Session */
        $session = $request->getSession();

        $authErrorKey = Security::AUTHENTICATION_ERROR;
        $lastUsernameKey = Security::LAST_USERNAME;

        // get the error if any (works with forward and redirect -- see below)
        if ($request->attributes->has($authErrorKey)) {
            $error = $request->attributes->get($authErrorKey);
        } elseif (null !== $session && $session->has($authErrorKey)) {
            $error = $session->get($authErrorKey);
            $session->remove($authErrorKey);
        } else {
            $error = null;
        }

        if (!$error instanceof AuthenticationException) {
            $error = null; // The value does not come from the security component.
        }

        // last username entered by the user
        $lastUsername = (null === $session) ? '' : $session->get($lastUsernameKey);

        $csrfToken = $this->tokenManager
            ? $this->tokenManager->getToken('authenticate')->getValue()
            : null;

        return $this->renderLogin(array(
            'last_username' => $lastUsername,
            'error' => $error,
            'csrf_token' => $csrfToken
        ));
    }

    /**
     * @param array $data
     * @return Response
     */
    protected function renderLogin(array $data)
    {
        return $this->render('auth/login.html.twig', $data);
    }

    public function checkAction()
    {
        return parent::checkAction();
    }

    /**
     * @return RedirectResponse
     */
    public function logoutAction()
    {
        return $this->redirectToRoute('home');
    }
}
