<?php


namespace App\Controller;

use App\Api\ApiResponse;
use App\Form\Type\ConfirmType;
use App\Form\Type\PaymentType;
use App\Service\PaymentApi;
use App\Service\RepoHelper;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Cookie;
use App\Entity\User;
use App\Entity\Payment;
use App\Entity\Balance;
use App\Entity\Currency;
use FOS\UserBundle\Model\UserInterface;

/**
 * Class PaymentController
 * @package App\Controller
 */
class PaymentController extends Controller
{

    /** @var PaymentApi $paymentApi */
    private $paymentApi;

    /** @var RepoHelper $repoHelper */
    private $repoHelper;

    /**
     * PaymentController constructor.
     * @param PaymentApi $paymentApi
     */
    public function __construct(PaymentApi $paymentApi, RepoHelper $repoHelper)
    {
        $this->paymentApi = $paymentApi;
        $this->repoHelper = $repoHelper;
    }

    /**
     * @param Request $request
     * @param string $currency
     * @return mixed
     * @throws \Exception
     */
    public function withdrawAction(Request $request, $currency)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        /** @var User $user */
        $user = $this->getUser();

        /** @var Currency $curr */
        $curr = $this->repoHelper->getCurrencyByTitle($currency);
        if (null == $curr) {
            throw new \Exception("This currency doesn't exist");
        }

        /** @var Balance $balance */
        $balance = $user->getBalanceByCurrency($curr);
        if (null == $balance) {
            throw new \Exception("You don't have balance in this currency");
        }

        /** @var Payment $payment */
        $payment = new Payment();
        $payment->setUser($user);
        $payment->setCurrency($curr);

        $form = $this->createForm(PaymentType::class, $payment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /** @var ApiResponse $response */
            $response = $this->paymentApi->createPayment($payment->getArrayData());

            if ($response->isSuccess()) {

                $payment->setTransactionId($response->getData('transaction_id'));
                $payment->setFee($response->getData('fee'));

                $this->repoHelper->savePayment($payment);

                return $this->redirect(
                    $this->generateUrl('confirm_payment', [
                            'transactionId' => $payment->getTransactionId()]
                    )
                );
            }

            $request->getSession()->getFlashBag()->add('withdraw_error', $response->getMessage());
        }

        return $this->render('payment/withdraw.html.twig', [
                'balance' => $balance,
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @param Request $request
     * @param string $transactionId
     * @return mixed
     * @throws \Exception
     */
    public function sendConfirmAction(Request $request, $transactionId)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        /** @var User $user */
        $user = $this->getUser();

        $form = $this->createForm(ConfirmType::class);
        $form->handleRequest($request);

        /** @var Payment $payment */
        $payment = $this->repoHelper->getPaymentByTransaction($transactionId);
        if (null == $payment) {
            throw new \Exception("The payment with this transaction Id doesn't exist");
        }

        if ($form->isSubmitted() && $form->isValid()) {

            /** @var ApiResponse $response */
            $response = $this->paymentApi->confirmPayment(
                ['transaction_id' => $transactionId, 'code' => $form->getData()['code']]
            );
            if ($response->isSuccess()) {

                $payment->setUpdatedDate(new \DateTime());
                $payment->setStatus(Payment::STATUS_CONFIRMED);

                $user->getBalanceByCurrency($payment->getCurrency())->lessBalance(
                    $payment->getAmount() + $payment->getFee()
                );

                $this->repoHelper->save();

                return $this->redirect($this->generateUrl('success_payment'));
            }

            $request->getSession()->getFlashBag()->add('code_error',
                $response->getMessage()
            );
        }
        return $this->render('payment/confirm.html.twig', [
                'transactionId' => $transactionId,
                'payment' => $payment,
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @return mixed
     */
    public function successAction()
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        /** @var User $user */
        $user = $this->getUser();

        return $this->render('payment/success.html.twig');
    }

    /**
     * @return mixed
     */
    public function paymentsAction()
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        /** @var User $user */
        $user = $this->getUser();

        return $this->render('payment/payments.html.twig', [
                'payments' => $user->getPayments()
            ]
        );
    }

    /**
     * @param string $transactionId
     * @return mixed
     * @throws \Exception
     */
    public function detailsAction($transactionId)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        /** @var User $user */
        $user = $this->getUser();

        /** @var Payment $payment */
        $payment = $this->repoHelper->getPaymentByTransaction($transactionId);

        if (null == $payment ) {
            throw new \Exception("The payment with this transaction Id doesn't exist");
        }
        if ($user->getId() != $payment->getUser()->getId() ) {
            throw new \Exception("The payment with this transaction Id doesn't exist");
        }

        return $this->render('payment/details.html.twig', [
                'payment' => $payment
            ]
        );
    }
}

