<?php


namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class BalanceController
 * @package App\Controller
 */
class BalanceController extends Controller
{
    /**
     * @return mixed
     */
    public function balanceAction()
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        /** @var User $user */
        $user = $this->getUser();

        return $this->render(
            'balance/balance.html.twig',
            [
                'balances' => $user->getBalances()
            ]
        );
    }

}

