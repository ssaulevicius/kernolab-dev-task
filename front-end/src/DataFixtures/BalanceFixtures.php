<?php

namespace App\DataFixtures;

use App\Entity\Balance;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
/**
 * Class BalanceFixtures
 * @package App\DataFixtures
 */
class BalanceFixtures extends Fixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

        $balance = new Balance();
        $balance->setUser($this->getReference(UserFixtures::USER1));
        $balance->setCurrency($this->getReference(CurrencyFixtures::CURRENCY_EUR));
        $balance->setAmount('5000');
        $manager->persist($balance);

        $balance = new Balance();
        $balance->setUser($this->getReference(UserFixtures::USER1));
        $balance->setCurrency($this->getReference(CurrencyFixtures::CURRENCY_GBP));
        $balance->setAmount('5000');
        $manager->persist($balance);

        $balance = new Balance();
        $balance->setUser($this->getReference(UserFixtures::USER2));
        $balance->setCurrency($this->getReference(CurrencyFixtures::CURRENCY_USD));
        $balance->setAmount('5000');
        $manager->persist($balance);

        $balance = new Balance();
        $balance->setUser($this->getReference(UserFixtures::USER2));
        $balance->setCurrency($this->getReference(CurrencyFixtures::CURRENCY_RUB));
        $balance->setAmount('5000');
        $manager->persist($balance);

        $manager->flush();
    }

    public function getOrder()
    {
        return 3;
    }

    public function getDependencies()
    {
        return array(
            UserFixtures::class,
            CurrencyFixtures::class
        );
    }
}
