<?php

namespace App\Form\Type;

use App\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Doctrine\ORM\EntityManager;
use App\Entity\Currency;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\GreaterThan;

/**
 * Class PaymentType
 * @package App\Form\Type
 */
class PaymentType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('receiverAccount', TextType::class, [
            'label' => false,
            'data' => '',
            'attr' => array('autocomplete' => 'off'),
            'constraints' => [new Length(['min' => 8])]
        ]);

        $builder->add('receiverName', TextType::class, [
            'label' => false,
            'data' => '',
            'attr' => array('autocomplete' => 'off'),
            'constraints' => [new Length(['min' => 2])]
        ]);

        $builder->add('amount', NumberType::class, [
            'label' => false,
            'data' => null,
            'attr' => array('autocomplete' => 'off'),
            'constraints' => [new GreaterThan(1)]
        ]);

        $builder->add('details', TextareaType::class, [
            'label' => false,
            'data' => '',
            'attr' => array('autocomplete' => 'off'),
        ]);

        $builder->remove('user');
        $builder->remove('transactionId');
        $builder->remove('currency');
        $builder->remove('fee');

        $builder->add('submit', SubmitType::class, [
            'attr' => array('class' => 'btn btn-primary btn-xl'),
            'label' => 'Withdraw',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Payment',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'payment';
    }
}
