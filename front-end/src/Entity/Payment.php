<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PaymentRepository")
 * @ORM\Table(name="payment")
 */
class Payment
{

    CONST STATUS_NOT_CONFIRMED = 1;
    CONST STATUS_CONFIRMED = 2;
    CONST STATUS_COMPLETED = 3;
    CONST STATUS_REJECTED = 4;

    CONST GATEWAY_STATUS_CONFIRMED = 2;
    CONST GATEWAY_STATUS_COMPLETED = 3;
    CONST GATEWAY_STATUS_REJECTED = 4;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @Assert\NotBlank()
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=255, unique=true, nullable=true)
     */
    private $transactionId;

    /**
     * @ORM\Column(type="float")
     */
    private $amount;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $fee;

    /**
     * @ORM\ManyToOne(targetEntity="Currency")
     * @Assert\NotBlank()
     */
    private $currency;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $receiverAccount;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $receiverName;

    /**
     * @ORM\Column(type="string")
     */
    private $details;

    /**
     * @ORM\Column(type="integer", options={"default": "1"})
     */
    private $status;

    /**
     * @ORM\Column(type="string", options={"default": ""}, nullable=true)
     */
    private $statusMsg;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedDate;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdDate;

    /**
     * Payment constructor.
     */
    public function __construct()
    {
        if (null == $this->createdDate) {
            $this->createdDate = new \Datetime();
        }
        if (null == $this->status) {
            $this->setStatus(self::STATUS_NOT_CONFIRMED);
        }
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return Payment
     */
    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTransactionId(): ?string
    {
        return $this->transactionId;
    }

    /**
     * @param string $transactionId
     * @return Payment
     */
    public function setTransactionId(string $transactionId): self
    {
        $this->transactionId = $transactionId;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getAmount(): ?float
    {
        return round($this->amount, 2);
    }

    /**
     * @param float $amount
     * @return Payment
     */
    public function setAmount(float $amount): self
    {
        $this->amount = round($amount, 2);

        return $this;
    }

    /**
     * @return float|null
     */
    public function getFee(): ?float
    {
        return round($this->fee, 2);
    }

    /**
     * @param float $fee
     * @return Payment
     */
    public function setFee(float $fee): self
    {
        $this->fee = round($fee, 2);

        return $this;
    }

    /**
     * @return Currency|null
     */
    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    /**
     * @param Currency $currency
     * @return Payment
     */
    public function setCurrency(Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getReceiverAccount(): ?string
    {
        return $this->receiverAccount;
    }

    /**
     * @param string|null $receiverAccount
     * @return Payment
     */
    public function setReceiverAccount(string $receiverAccount): self
    {
        $this->receiverAccount = $receiverAccount;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getReceiverName(): ?string
    {
        return $this->receiverName;
    }

    /**
     * @param string $receiverName
     * @return Payment
     */
    public function setReceiverName(string $receiverName): self
    {
        $this->receiverName = $receiverName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDetails(): ?string
    {
        return $this->details;
    }

    /**
     * @param string $details
     * @return Payment
     */
    public function setDetails(string $details): self
    {
        $this->details = $details;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return Payment
     */
    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getStatusMsg(): ?string
    {
        return $this->statusMsg;
    }

    /**
     * @param string $statusMsg
     * @return Payment
     */
    public function setStatusMsg(string $statusMsg): self
    {
        $this->statusMsg = $statusMsg;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getUpdatedDate(): ?\DateTimeInterface
    {
        return $this->updatedDate;
    }

    /**
     * @param \DateTimeInterface $updatedDate
     * @return Payment
     */
    public function setUpdatedDate(\DateTimeInterface $updatedDate): self
    {
        $this->updatedDate = $updatedDate;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getCreatedDate(): ?\DateTimeInterface
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTimeInterface $createDated
     * @return Payment
     */
    public function setCreatedDate(\DateTimeInterface $createDated): self
    {
        $this->createdDate = $createDated;

        return $this;
    }

    /**
     * @return bool
     */
    public function isNotConfirmed()
    {
        return $this->status == self::STATUS_NOT_CONFIRMED;
    }

    /**
     * @return bool
     */
    public function isConfirmed()
    {
        return $this->status == self::STATUS_CONFIRMED;
    }

    /**
     * @return bool
     */
    public function isCompleted()
    {
        return $this->status == self::STATUS_COMPLETED;
    }

    /**
     * @return bool
     */
    public function isRejected()
    {
        return $this->status == self::STATUS_REJECTED;
    }

    /**
     * @return string
     */
    public function getStatusByText()
    {
        switch ($this->status) {
            case self::STATUS_NOT_CONFIRMED:
                return 'Not confirmed';
            case self::STATUS_CONFIRMED:
                return 'Confirmed';
            case self::STATUS_COMPLETED:
                return 'Completed';
            case self::STATUS_REJECTED:
                return 'Rejected';
        }
    }

    /**
     * @return array
     */
    public function getArrayData()
    {
        return [
            'user_id' => $this->getUser()->getId(),
            'receiver_account' => $this->getReceiverAccount(),
            'receiver_name' => $this->getReceiverName(),
            'amount' => $this->getAmount(),
            'currency' => $this->getCurrency()->getTitle(),
            'details' => $this->getDetails(),
        ];
    }
}
