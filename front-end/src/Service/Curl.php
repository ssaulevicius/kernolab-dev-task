<?php

namespace App\Service;

class Curl
{
    /**
     * @param string $url
     * @param string $authorization
     * @return bool|mixed
     */
    public function getRequest($url, $authorization = '')
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_CONNECTTIMEOUT => 10,
            CURLOPT_TIMEOUT => 20,
            CURLOPT_USERAGENT => 'Mozilla/5.0 (Android 4.4; Mobile; rv:41.0) Gecko/41.0 Firefox/41.0',
            CURLOPT_HTTPHEADER => array(
                'Authorization: ' . $authorization,
                'Content-Type: application/json'
            )
        ));

        if (!($resp = curl_exec($curl))) {
            return false;
        }

        curl_close($curl);

        return $resp;
    }

    /**
     * @param string $url
     * @param string $body
     * @param string $authorization
     * @return bool|string
     */
    public function postRequest($url, $body, $authorization = '')
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_FOLLOWLOCATION => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_HEADER => false,
            CURLINFO_HEADER_OUT => 1,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $body,
            CURLOPT_URL => $url,
            CURLOPT_CONNECTTIMEOUT => 10,
            CURLOPT_TIMEOUT => 20,
            CURLOPT_USERAGENT => 'Mozilla/5.0 (Android 4.4; Mobile; rv:41.0) Gecko/41.0 Firefox/41.0',
            CURLOPT_HTTPHEADER => array(
                'Authorization: ' . $authorization,
                'Content-Type: application/json',
                'Content-Length: ' . strlen($body)
            )
        ));

        if (!($response = curl_exec($curl))) {

            $error_msg = '';

            if (curl_errno($curl)) {
                $error_msg = curl_error($curl);
            }

            return json_encode(['success' => false, 'message' => $error_msg]);
        }

        curl_close($curl);

        return $response;
    }

    /**
     * @param string $url
     * @param string $body
     * @param string $authorization
     * @return bool|string
     */
    public function pathRequest($url, $body, $authorization = '')
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
                CURLOPT_FOLLOWLOCATION => 1,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_SSL_VERIFYHOST => 0,
                CURLOPT_HEADER => false,
                CURLINFO_HEADER_OUT => 1,
                CURLOPT_CUSTOMREQUEST => 'PATCH',
                CURLOPT_POSTFIELDS => $body,
                CURLOPT_URL => $url,
                CURLOPT_CONNECTTIMEOUT => 10,
                CURLOPT_TIMEOUT => 20,
                CURLOPT_USERAGENT => 'Mozilla/5.0 (Android 4.4; Mobile; rv:41.0) Gecko/41.0 Firefox/41.0',
                CURLOPT_HTTPHEADER => array(
                    'Authorization: ' . $authorization,
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($body)
            )
        ));

        if (!($response = curl_exec($curl))) {

            $error_msg = '';

            if (curl_errno($curl)) {
                $error_msg = curl_error($curl);
            }

            return json_encode(['success' => false, 'message' => $error_msg]);
        }

        curl_close($curl);

        return $response;
    }
}
