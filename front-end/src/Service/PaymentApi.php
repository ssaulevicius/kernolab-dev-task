<?php

namespace App\Service;

use App\Api\ApiResponse;
use App\Entity\Payment;
use App\Repository\PaymentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

/**
 * Class PaymentApi
 * @package App\Service
 */
class PaymentApi
{
    /** @var array */
    CONST REQUIRED_FULL_FIELDS = [
        'transaction_id',
        'receiver_account',
        'receiver_name',
        'amount',
        'fee',
        'details',
        'status'
    ];

    /** @var array */
    CONST REQUIRED_CREATED_FIELDS = [
        'transaction_id',
        'fee'
    ];

    /**
     * @var Curl
     */
    private $curl;

    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $authToken;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /** @var PaymentRepository */
    private $paymentRepository;

    /** @var EntityManagerInterface */
    private $em;

    /** @var ApiResponse */
    private $response;

    /**
     * Conv constructor.
     * @param Curl $curl
     * @param string $url
     * @param string $authToken
     */
    public function __construct(
        Curl $curl,
        String $url,
        String $authToken,
        LoggerInterface $logger,
        PaymentRepository $paymentRepository,
        EntityManagerInterface $em
    )
    {
        $this->curl = $curl;
        $this->url = $url;
        $this->authToken = $authToken;
        $this->logger = $logger;
        $this->paymentRepository = $paymentRepository;
        $this->em = $em;
        $this->response = new ApiResponse();
    }

    /**
     * @param string $addMsg
     */
    private function writeLog($addMsg = '')
    {
        if (!$this->response->isSuccess()) {
            $this->logger->critical($this->response->getMessage(), [$addMsg]);
        } else {
            $this->logger->info('success', [$addMsg, $this->response->getAllData()]);
        }
    }

    /**
     * @param string $method
     * @param string $url
     * @param array $data
     * @param array $returnRequiredFields
     * @return mixed
     */
    private function request($method, $url, $data)
    {
        switch ($method) {
            case 'get':
                $response = $this->curl->getRequest($url, $this->authToken);
                break;
            case 'post':
                $response = $this->curl->postRequest($url, json_encode($data), $this->authToken);
                break;
            case 'patch':
                $response = $this->curl->pathRequest($url, json_encode($data), $this->authToken);
                break;
        }

        return json_decode($response, true);
    }

    /**
     * @param $transactionId
     * @return ApiResponse
     */
    public function getPaymentInfo($transactionId)
    {
        $url = $this->url . '/payment/' . $transactionId;

        $this->response->add(
            $this->request('get', $url, []),
            self::REQUIRED_FULL_FIELDS
        );

        $this->writeLog($url);

        return $this->response;
    }

    /**
     * @param $data
     * @return ApiResponse
     */
    public function createPayment($data)
    {
        $url = $this->url . '/payment';

        $this->response->add(
            $this->request('post', $url, $data),
            self::REQUIRED_CREATED_FIELDS
        );

        $this->writeLog($url);

        return $this->response;
    }

    /**
     * @param $data
     * @return ApiResponse
     */
    public function confirmPayment($data)
    {
        $url = $this->url . '/payment/confirm';

        $this->response->add(
            $this->request('patch', $url, $data),
            []
        );

        $this->writeLog($url);

        return $this->response;
    }

    /**
     * @return ApiResponse
     */
    public function getReponse()
    {
        return $this->response;
    }
}
