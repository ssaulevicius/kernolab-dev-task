<?php

namespace App\Service;

use App\Entity\Currency;
use App\Entity\Payment;
use App\Repository\CurrencyRepository;
use App\Repository\PaymentRepository;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class RepoHelper
 * @package App\Service
 */
class RepoHelper
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * @var PaymentRepository
     */
    private $paymentRepository;

    /**
     * @var CurrencyRepository
     */
    private $currencyRepository;

    /**
     * RepositoryHelper constructor.
     *
     * @param PaymentRepository $paymentRepository
     * @param CurrencyRepository $currencyRepository
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        PaymentRepository $paymentRepository,
        CurrencyRepository $currencyRepository)
    {
        $this->entityManager = $entityManager;
        $this->paymentRepository = $paymentRepository;
        $this->currencyRepository = $currencyRepository;
    }

    /**
     * @param $transactionId
     * @return Payment|null
     */
    public function getPaymentByTransaction($transactionId)
    {
        return $this->paymentRepository->findOneBy(['transactionId' => $transactionId]);
    }

    /**
     * @param Payment $payment
     */
    public function savePayment(Payment $payment)
    {
        $this->entityManager->persist($payment);
        $this->entityManager->flush();
    }

    public function save()
    {
        $this->entityManager->flush();
    }

    /**
     * @param $title
     * @return Currency|null
     */
    public function getCurrencyByTitle($title)
    {
        return $this->currencyRepository->findOneBy(['title' => $title]);
    }
}
