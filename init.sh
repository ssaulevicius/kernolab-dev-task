#!/bin/bash

# init front-end
docker exec -it sf_php /bin/bash -c 'cd front-end; composer install'
docker exec -it sf_php /bin/bash -c 'cd front-end; php bin/console doctrine:database:drop --force --if-exists'
docker exec -it sf_php /bin/bash -c 'cd front-end; php bin/console doctrine:database:create --if-not-exists'
docker exec -it sf_php /bin/bash -c 'cd front-end; php bin/console doctrine:schema:update --force'
docker exec -it sf_php /bin/bash -c 'cd front-end; php bin/console doctrine:fixtures:load --append'
docker exec -it sf_php /bin/bash -c 'cd front-end; yarn install'
docker exec -it sf_php /bin/bash -c 'cd front-end; yarn encore dev'
docker exec -it sf_php /bin/bash -c 'cd front-end; php bin/console cache:clear'

# init rest
docker exec -it sf_php /bin/bash -c 'cd rest-api; composer install'
docker exec -it sf_php /bin/bash -c 'cd rest-api; php bin/console doctrine:database:drop --force --if-exists'
docker exec -it sf_php /bin/bash -c 'cd rest-api; php bin/console doctrine:database:create --if-not-exists'
docker exec -it sf_php /bin/bash -c 'cd rest-api; php bin/console doctrine:schema:update --force'
docker exec -it sf_php /bin/bash -c 'cd rest-api; php bin/console doctrine:fixtures:load --append'
docker exec -it sf_php /bin/bash -c 'cd rest-api; php bin/console cache:clear'

