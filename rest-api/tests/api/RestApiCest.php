<?php

/**
 * Class RestApiCest
 */
class RestApiCest
{

    /**
     * @var string
     */
    private $userId = '58558558';

    /**
     * @var string
     */
    private $confirmCode = '111';

    /**
     * @var string
     */
    private $authToken = '0a9df217ef6c409e13d7e20fd7642d66';

    /**
     * @var array
     */
    private $postData = [
        'user_id' => '58558558',
        'receiver_account' => 'LT777300010001969710',
        'receiver_name' => 'Sigitas Saulevicius',
        'amount' => 11.11,
        'details' => 'pavedimas',
        'currency' => 'eur'
    ];

    /**
     * @var
     */
    private $transactionId;

    /**
     * @param ApiTester $I
     */
    public function _before(ApiTester $I)
    {
    }

    /**
     * @param ApiTester $I
     */
    private function deleteFromDb(ApiTester $I)
    {
        $I->canSeeInDatabase('payment', ['user_id' => $this->userId]);
        $I->deleteFromDatabase('payment', ['user_id' => $this->userId]);
    }

    /**
     * @param ApiTester $I
     */
    public function sendPostJsonFailTest(\ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/x-www-form-urlencoded');
        $I->sendPOST('/payment', $this->postData);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::BAD_REQUEST);
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"success":false,"message":"Request must be in JSON format"}');
    }

    /**
     * @param ApiTester $I
     */
    public function sendPostAuthorizationFailTest(\ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->haveHttpHeader('Authorization', '');
        $I->sendPOST('/payment', $this->postData);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::BAD_REQUEST);
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"success":false,"message":"Wrong authorization header"}');
    }

    /**
     * @param ApiTester $I
     */
    public function sendPostFailTest(\ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->haveHttpHeader('Authorization', $this->authToken);
        $I->sendPOST('/payment', []);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::BAD_REQUEST);
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"success":false,"message":"Missing required field(s) or incorrect data"}');
    }

    /**
     * @param ApiTester $I
     * @return ApiTester
     */
    public function sendPostSuccessTest(\ApiTester $I, $newAmount = 0)
    {
        if ($newAmount > 0) {
            $this->postData['amount'] = (float)round($newAmount, 2);
        }
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->haveHttpHeader('Authorization', $this->authToken);
        $I->sendPOST('/payment', $this->postData);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
        $I->seeResponseContains('success":true');
        $I->seeResponseContains('transaction_id');
        $response = json_decode($I->grabResponse(), true);

        $this->transactionId = $response['data']['transaction_id'];

        return $I;
    }

    /**
     * @param ApiTester $I
     */
    public function confirmPaymentFailTest(\ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->haveHttpHeader('Authorization', $this->authToken);
        $I->sendPATCH('/payment/confirm', ['']);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::BAD_REQUEST);
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"success":false,"message":"There are no payments by this ID"}');
    }

    /**
     * @param ApiTester $I
     */
    public function confirmPaymentTest(\ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->haveHttpHeader('Authorization', $this->authToken);
        $I->sendPATCH('/payment/confirm',
            ['transaction_id' => $this->transactionId, 'code' => $this->confirmCode]
        );
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::ACCEPTED);
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"success":true,"data":[]}');
    }

    /**
     * @param ApiTester $I
     */
    public function getPaymentInfoFailTest(\ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->haveHttpHeader('Authorization', $this->authToken);
        $I->sendGET('/payment/' . uniqid('codeception' . time() . rand(0, 1000)));
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::BAD_REQUEST);
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"success":false,"message":"There are no payments by this ID"}');
    }

    /**
     * @param ApiTester $I
     */
    public function getPaymentInfoSuccessTest(\ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->haveHttpHeader('Authorization', $this->authToken);
        $I->sendGET('/payment/' . $this->transactionId);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContains('success":true');
        $I->seeResponseContains('transaction_id');
        $I->seeResponseContains('receiver_account');
        $I->seeResponseContains('receiver_name');
        $I->seeResponseContains('amount');
        $I->seeResponseContains('details');
        $I->seeResponseContains('currency');
        $I->seeResponseContains('status');
    }

    /**
     * @param ApiTester $I
     */
    public function limitPerHourTest(\ApiTester $I)
    {
        // value set 2 because one was done above
        $x = 2;
        do {
            $this->sendPostSuccessTest($I);
            $x++;
        } while ($x <= 10);

        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->haveHttpHeader('Authorization', $this->authToken);
        $I->sendPOST('/payment', $this->postData);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::BAD_REQUEST);
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"success":false,"message":"Your reached hourly limit for transactions"}');

        $this->deleteFromDb($I);
    }

    /**
     * @param ApiTester $I
     */
    public function feePercentTest(\ApiTester $I)
    {
        // first transaction get 10% fee
        $I = $this->sendPostSuccessTest($I);
        $response = json_decode($I->grabResponse(), true);
        $I->assertEquals('1.11', $response['data']['fee']);

        // second transaction get 10% fee
        $I = $this->sendPostSuccessTest($I, 100);
        $response = json_decode($I->grabResponse(), true);
        $I->assertEquals('10', $response['data']['fee']);

        // third transaction get 5% fee
        $I = $this->sendPostSuccessTest($I, 40);
        $response = json_decode($I->grabResponse(), true);
        $I->assertEquals('2', $response['data']['fee']);

        $this->deleteFromDb($I);
    }

    /**
     * @param ApiTester $I
     */
    public function maxAmountPerUserTest(\ApiTester $I)
    {
        // first transaction get 10% fee
        $this->sendPostSuccessTest($I, 100);

        $this->postData['amount'] = 1000;
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->haveHttpHeader('Authorization', $this->authToken);
        $I->sendPOST('/payment', $this->postData);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::BAD_REQUEST);
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"success":false,"message":"Your all amounts (include this) reached transfer limit in this currency"}');

        $this->deleteFromDb($I);
    }
}
