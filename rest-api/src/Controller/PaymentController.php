<?php

namespace App\Controller;

use App\Entity\Currency;
use App\Entity\Settings;
use App\Form\Type\PaymentType;
use App\Service\PaymentResponse;
use App\Service\PaymentService;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use App\Entity\Payment;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Form\FormInterface;

/**
 * Payment controller.
 *
 * @Route("/api")
 */
class PaymentController extends Controller
{

    /** @var PaymentService */
    private $paymentService;

    /** @var PaymentResponse */
    private $paymentResponse;

    /**
     * PaymentController constructor.
     * @param PaymentService $paymentService
     * @param PaymentResponse $paymentResponse
     */
    public function __construct(
        PaymentService $paymentService,
        PaymentResponse $paymentResponse)
    {
        $this->paymentService = $paymentService;
        $this->paymentResponse = $paymentResponse;
    }

    /**
     * Get Payment.
     * @FOSRest\Get("/payment/{transactionId}")
     *
     * @param int $transactionId
     * @return JsonResponse
     * @throws \Exception
     */
    public function getPaymentAction(Request $request, $transactionId)
    {
        $this->paymentService->addRequest($request, ['transaction_id' => $transactionId]);

        if (!$this->paymentService->isJsonRequest()) {
            return $this->paymentResponse->badJsonFormat();
        }

        if (!$this->paymentService->isValidAuthorization()) {
            return $this->paymentResponse->badAuthorization();
        }

        $payment = $this->paymentService->getPayment();
        if (null == $payment) {
            return $this->paymentResponse->badNotFoundByTransaction();
        }

        return $this->paymentResponse->data($payment->getArrayData());
    }

    /**
     * Create Payment.
     * @FOSRest\Post("/payment")
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function postPaymentAction(Request $request)
    {
        $this->paymentService->addRequest($request);

        if (!$this->paymentService->isJsonRequest()) {
            return $this->paymentResponse->badJsonFormat();
        }

        if (!$this->paymentService->isValidAuthorization()) {
            return $this->paymentResponse->badAuthorization();
        }

        $payment = $this->paymentService->getPayment();

        if (null == $payment) {
            return $this->paymentResponse->badMissingField();
        }

        if (!$this->paymentService->validateHourlyPerUser()) {
            return $this->paymentResponse->badHourlyLimit();
        }

        if (!$this->paymentService->validateMaxAmountPerUser()) {
            return $this->paymentResponse->badTransferLimit();
        }

        if (!$this->paymentService->savePayment()) {
            return $this->paymentResponse->badSomethingWrong();
        };

        return $this->paymentResponse->created($payment->getCreatedData());
    }

    /**
     * Confirm Payment by 2FA code
     *
     * @FOSRest\Patch("/payment/confirm")
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function pathPaymentAction(Request $request)
    {
        $this->paymentService->addRequest($request);

        if (!$this->paymentService->isJsonRequest()) {
            return $this->paymentResponse->badJsonFormat();
        }

        if (!$this->paymentService->isValidAuthorization()) {
            return $this->paymentResponse->badAuthorization();
        }

        $payment = $this->paymentService->getPayment();
        if (null == $payment) {
            return $this->paymentResponse->badNotFoundByTransaction();
        }

        if (!$this->paymentService->isValid2FACode()) {
            return $this->paymentResponse->bad2FACode();
        }

        if (!$this->paymentService->confirmPayment()) {
            return $this->paymentResponse->badSomethingWrong();
        };

        return $this->paymentResponse->updated();
    }
}