<?php

namespace App\Api;

class ApiResponse
{
    /** @var string */
    CONST DEFAULT_ERROR_MSG = 'Sorry, payment service currently not available';

    /**
     * @var bool
     */
    private $success = false;

    /**
     * @var array
     */
    private $data = [];

    /**
     * @var string
     */
    private $message = '';

    /**
     * @param array $contentData
     * @param array $requiredFields
     * @return bool
     */
    private function isValidFields($contentData, $requiredFields)
    {
        return (count(
                array_intersect_key(
                    array_flip($requiredFields), $contentData)) === count($requiredFields)
        );
    }

    /**
     * ApiResponse constructor.
     * @param $response
     * @param array $requiredFields
     */
    public function add($response, $requiredFields = [])
    {
        if (isset($response['success'])) {
            $this->success = (boolean)$response['success'];
        }

        if (isset($response['data']) && count($response['data']) > 0) {
            $this->data = $response['data'];
        }

        if ($this->success && $this->data && count($requiredFields) > 0) {
            $this->success = $this->isValidFields($this->data, $requiredFields);
        }

        if (!$this->success) {
            if (isset($response['message'])) {
                $this->message = $response['message'];
            } else {
                $this->message = self::DEFAULT_ERROR_MSG;
            }
        }
    }

    /**
     * @return bool
     */
    public function isSuccess()
    {
        return $this->success == true;
    }

    /**
     * @param $field
     * @return null
     */
    public function getData($field)
    {
        if (isset($this->data[$field])) {
            return $this->data[$field];
        }
        return null;
    }

    /**
     * @return mixed
     */
    public function getAllData()
    {
        return $this->data;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

}
