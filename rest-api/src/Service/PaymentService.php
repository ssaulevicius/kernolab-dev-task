<?php

namespace App\Service;

use App\Entity\Currency;
use App\Entity\Payment;
use App\Entity\Settings;
use App\Repository\PaymentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class PaymentService
 * @package App\Service
 */
class PaymentService
{
    /** @var array */
    CONST REQUIRED_FIELDS = ['user_id', 'receiver_account', 'receiver_name', 'amount', 'details', 'currency'];

    /** @var int */
    CONST MAX_AMOUNT = 1;

    /** @var Request */
    private $request;

    /** @var array */
    private $content = [];

    /** @var Payment */
    private $payment;

    /** @var ValidatorInterface */
    private $validator;

    /** @var Settings */
    private $settings;

    /** @var PaymentRepository */
    private $paymentRepository;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * PaymentService constructor.
     * @param EntityManagerInterface $em
     * @param ValidatorInterface $validator
     */
    public function __construct(
        EntityManagerInterface $em,
        ValidatorInterface $validator,
        ParameterBagInterface $parameter
    )
    {
        $this->em = $em;
        $this->validator = $validator;
        $this->paymentRepository = $this->em->getRepository(Payment::class);
        $this->settings = $this->em->getRepository(Settings::class)->findOneBy(
            ['version' => $parameter->get('settings_version')]
        );
    }

    /**
     * @param float $amount
     * @param float $currentDayAmount
     * @param array $feeRules
     * @return float|int
     */
    private function calculateFee(float $amount, float $currentDayAmount, array $feeRules)
    {
        if (0 == count($feeRules) || 0 == $amount) {
            return 0;
        }

        foreach ($feeRules as $sum => $percent) {
            if ($currentDayAmount <= $sum) {
                return round($amount * $percent / 100, 2);
            }
        }

        return 0;
    }

    /**
     * @param $contentData
     * @param $requiredFields
     * @return bool
     */
    private function isValidFields($contentData, $requiredFields)
    {
        return (count(
                array_intersect_key(
                    array_flip($requiredFields), $contentData)) === count($requiredFields)
        );
    }

    /**
     * @return Payment|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    private function getPaymentFromRequest()
    {
        if (!$this->isValidFields($this->content, self::REQUIRED_FIELDS)) {
            return null;
        }

        if ((float)$this->content['amount'] < self::MAX_AMOUNT) {
            return null;
        }

        /** @var Currency $currency */
        $currency = $this->em->getRepository(Currency::class)->findOneBy([
                'title' => $this->content['currency']
            ]
        );
        if (null == $currency) {
            return null;
        }

        $payment = new Payment();
        $payment->setUserId($this->content['user_id']);
        $payment->setReceiverAccount($this->content['receiver_account']);
        $payment->setReceiverName($this->content['receiver_name']);
        $payment->setAmount($this->content['amount']);
        $payment->setDetails($this->content['details']);
        $payment->setCurrency($currency);
        $payment->setProvider($currency->getProvider());
        $payment->setTransactionId(uniqid($payment->getUserId() . time() . rand(0, 1000)));
        $payment->setFee(
            $this->calculateFee(
                (float)$payment->getAmount(),
                (float)$this->paymentRepository->getCurrentDayAmountPerUser($payment->getUserId()),
                unserialize($this->settings->getDailyFeeRules())
            )
        );

        $errors = $this->validator->validate($payment);
        if (count($errors) > 0) {
            return null;
        }

        return $payment;
    }

    /**
     * @param integer $transactionId
     * @return Payment|null
     */
    private function getPaymentByTransaction($transactionId)
    {
        return $this->paymentRepository->findOneBy(['transactionId' => $transactionId]);
    }

    /**
     * @return Payment
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * @return bool
     */
    public function savePayment()
    {
        if (null == $this->payment) {
            return false;
        }
        $this->em->persist($this->payment);
        $this->em->flush();

        return true;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function confirmPayment()
    {
        if (null == $this->payment) {
            return false;
        }
        $this->payment->setStatus(Payment::STATUS_CONFIRMED);
        $this->payment->setUpdatedDate(new \DateTime());
        $this->em->flush();

        return true;
    }

    /**
     * @param Request $request
     * @param array $customData
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function addRequest(Request $request, $customData = [])
    {
        $this->request = $request;
        if ($data = json_decode($this->request->getContent(), true)) {
            $this->content = $data;
        }
        if (count($customData) > 0) {
            $this->content = array_merge($this->content, $customData);
        }
        if (isset($this->content['transaction_id'])) {
            $this->payment = $this->getPaymentByTransaction($this->content['transaction_id']);
        } else {
            $this->payment = $this->getPaymentFromRequest();
        }
    }

    /**
     * @return bool
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function validateHourlyPerUser()
    {
        if (null == $this->payment) {
            return false;
        }
        $totalPayments = (int)$this->paymentRepository->getHourlyCountPerUser($this->payment->getUserId());
        return $totalPayments < $this->settings->getMaxTransactionsPerHour();
    }

    /**
     * @return bool
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function validateMaxAmountPerUser()
    {
        if (null == $this->payment) {
            return false;
        }
        $totalAmount = (float)$this->paymentRepository->getTotalAmountPerUser(
            $this->payment->getUserId(), $this->payment->getCurrency()
        );
        return $totalAmount + $this->payment->getAmount() <= $this->settings->getMaxAmountPerUser();
    }

    /**
     * @return bool
     */
    public function isValidAuthorization()
    {
        return $this->request->headers->get('Authorization') == $this->settings->getApiToken();
    }

    /**
     * @return bool
     */
    public function isJsonRequest()
    {
        return (false !== strpos($this->request->headers->get('Content-Type'), 'application/json'));
    }

    /**
     * @return bool
     */
    public function isValid2FACode()
    {
        if (isset($this->content['code'])) {
            return $this->content['code'] == $this->settings->getCode();
        }

        return false;
    }
}
