<?php

namespace App\Command;

use App\Entity\Payment;
use App\Service\PaymentProvider;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class Send
 * @package App\Command
 */
class Send extends Command
{

    protected function configure()
    {
        $this
            ->setName('provider:send')
            ->setDescription('Send money by payment gateway');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->newLine();
        $io->title('Starting send transactions');

        /** @var PaymentProvider $paymentProvider */
        $paymentProvider = $this->getApplication()->getKernel()->getContainer()->get('paymentProvider');

        /** @var EntityManagerInterface $em */
        $em = $this->getApplication()->getKernel()->getContainer()->get('doctrine')->getEntityManager();

        $confirmedPayments = $em->getRepository(Payment::class)->findBy(['status' => Payment::STATUS_CONFIRMED]);

        /** @var Payment $payment */
        foreach ($confirmedPayments as $payment) {

            $io->section('TRANSACTION ID ' . $payment->getTransactionId());

            $response = $paymentProvider->sendTransfer(
                $payment->getCurrency()->getProvider(), $payment->getArrayData()
            );

            if ($response->isSuccess()) {
                $payment->setStatus(Payment::STATUS_COMPLETED);
                $io->success('--- COMPLETED ---');
            } else {
                $payment->setStatus(Payment::STATUS_REJECTED);
                $io->comment('--- REJECTED ---');
            }

            $io->listing($response->getAllData());

            $payment->setDetails($payment->getDetails() . PHP_EOL . PHP_EOL . '#' . $response->getData('details'));
            $payment->setUpdatedDate(new \DateTime());
            $em->flush();
        }
    }
}
